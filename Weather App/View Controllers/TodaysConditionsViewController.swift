//
//  TodaysConditionsViewController.swift
//  Weather App
//
//  Created by Dean Mollica on 31/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class TodaysConditionsViewController: UIViewController, WeatherDataDelegate {
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var highTemperatureLabel: UILabel!
    @IBOutlet weak var lowTemperatureLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var favouriteButton: FavouriteButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WeatherDataController.shared.delegate = self
        updateUI()
    }
    
    
    func updateUI() {
        locationLabel.text = "Loading..."
        lastUpdatedLabel.text = "Last Updated: Never"
        weatherImageView.image = UIImage(named: "default")
        highTemperatureLabel.text = "--º"
        lowTemperatureLabel.text = "--º"
        descriptionLabel.text = ""
        windDirectionLabel.text = "--º"
        windSpeedLabel.text = "-- km/h"
        dateLabel.text = "Unknown"
        favouriteButton.isFavourite = false
        
        if WeatherDataController.shared.isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
        
        guard let dailyWeather = WeatherDataController.shared.weatherData?.dailyWeather?.data.first,
            let timezone = WeatherDataController.shared.weatherData?.timezone else {
                if let error = WeatherDataController.shared.error {
                    self.locationLabel.text = "No Data"
                    self.displayError(error.localizedDescription)
                }
                return
        }
        
        if let location = WeatherDataController.shared.location {
            locationLabel.text = location.name
        }
        
        let time = Date(timeIntervalSince1970: TimeInterval(dailyWeather.time))
        let lastUpdated = WeatherDataController.shared.formatLastUpdated(forDate: time)
        lastUpdatedLabel.text = "Last Updated: \(lastUpdated)"
        
        let todayDate = WeatherDataController.shared.formatDailyDate(forDate: time, inTimeZone: TimeZone(identifier: timezone)!)
        dateLabel.text = todayDate
        
        if let imageName = dailyWeather.imageName {
            weatherImageView.image = UIImage(named: imageName)
        }
        
        if let highTemperature = dailyWeather.highTemperature {
            highTemperatureLabel.text = "\(Int(highTemperature))º"
        }
        
        if let lowTemperature = dailyWeather.lowTemperature {
            lowTemperatureLabel.text = "\(Int(lowTemperature))º"
        }
        
        if let description = dailyWeather.description {
            descriptionLabel.text = description
        }
        
        if let windDirection = dailyWeather.windBearing {
            windDirectionLabel.text = "\(windDirection)º"
        }
        
        if let windSpeed = dailyWeather.windSpeed {
            windSpeedLabel.text = String(format: "%.1f km/h", windSpeed)
        }
        
        if let favourite = WeatherDataController.shared.favourite,
            let location = WeatherDataController.shared.location {
            favouriteButton.isFavourite = favourite.type == .today && favourite.location == location
        }
        
    }
    
    func displayError(_ error: String) {
        let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true)
    }
    
    func weatherDataDidFinishUpdate() {
        updateUI()
    }
    
    
    @IBAction func favouriteButtonTapped(_ sender: UIButton) {
        let feedbackGenerator = UISelectionFeedbackGenerator()
        feedbackGenerator.prepare()
        feedbackGenerator.selectionChanged()
        if sender.isSelected {
            WeatherDataController.shared.removeFavourite()
        } else {
            WeatherDataController.shared.setFavourite(forType: .today)
        }
        updateUI()
    }
    
    @IBAction func refreshButtonTapped(_ sender: UIBarButtonItem) {
        WeatherDataController.shared.updateWeatherData()
    }
    
}
