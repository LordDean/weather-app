//
//  ForecastTableViewController.swift
//  Weather App
//
//  Created by Dean Mollica on 31/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class ForecastTableViewController: UITableViewController {
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            let location = WeatherDataController.shared.location?.name ?? "Unknown"
            return "Select forecast for \(location)"
    }
}
