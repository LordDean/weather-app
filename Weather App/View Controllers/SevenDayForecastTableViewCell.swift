//
//  SevenDayForecastTableViewCell.swift
//  Weather App
//
//  Created by Dean Mollica on 6/8/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class SevenDayForecastTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var highTemperatureLabel: UILabel!
    @IBOutlet weak var lowTemperatureLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    
}
