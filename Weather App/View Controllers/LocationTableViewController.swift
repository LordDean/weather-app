//
//  LocationTableViewController.swift
//  Weather App
//
//  Created by Dean Mollica on 31/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class LocationTableViewController: UITableViewController {
    @IBOutlet weak var unitsButton: UIBarButtonItem!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switch WeatherDataController.shared.units {
        case .metric:
            unitsButton.title = "ºC"
        case .imperial:
            unitsButton.title = "ºF"
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Locations.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Select a location" : nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath)
        let location = Locations[indexPath.row]
        cell.textLabel?.text = location.name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func unitsButtonTapped(_ sender: UIBarButtonItem) {
        if WeatherDataController.shared.units == .metric {
            WeatherDataController.shared.setUnits(.imperial)
            sender.title = "ºF"
        } else {
            WeatherDataController.shared.setUnits(.metric)
            sender.title = "ºC"
        }
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LocationChosenSegue" {
            WeatherDataController.shared.location = Locations[tableView.indexPathForSelectedRow!.row]
        }
    }
    
}
