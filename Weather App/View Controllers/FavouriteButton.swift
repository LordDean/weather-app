//
//  FavouriteButton.swift
//  Weather App
//
//  Created by Dean Mollica on 11/8/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class FavouriteButton: UIButton {
    var isFavourite: Bool = false {
        didSet {
            if isFavourite {
                self.backgroundColor = UIColor(named: "iOSSystemYellow")
                self.tintColor = .black
            } else {
                self.backgroundColor = UIColor(named: "iOSSystemBlue")
                self.tintColor = .white
            }
            self.isSelected = self.isFavourite
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = self.frame.size.width / 2
        self.addTarget(self, action: #selector(animatePress), for: .touchDown)
        self.addTarget(self, action: #selector(animateRelease), for: [.touchDragExit, .touchCancel, .touchUpInside])
    }
    
    
    @IBAction private func animatePress(sender: FavouriteButton) {
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        }, completion: nil)
    }
    
    @IBAction private func animateRelease(sender: FavouriteButton) {
        UIView.animate(withDuration: 0.1, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }) { (_) in
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options: .curveEaseOut, animations: {
                sender.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    }
    
}
