//
//  SevenDayForecastTableViewController.swift
//  Weather App
//
//  Created by Dean Mollica on 31/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class SevenDayForecastTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, WeatherDataDelegate {
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var favouriteButton: FavouriteButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var forecasts: [WeatherDataPoint] = []
    var timezone: TimeZone?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WeatherDataController.shared.delegate = self
        tableView.allowsSelection = false
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 140, right: 0)
        updateUI()
    }
    
    
    func updateUI() {
        locationLabel.text = "Loading..."
        favouriteButton.isFavourite = false
        
        if WeatherDataController.shared.isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
        
        if let dailyWeatherData = WeatherDataController.shared.weatherData?.dailyWeather?.data,
            let timezoneString = WeatherDataController.shared.weatherData?.timezone {
            forecasts = Array(dailyWeatherData.dropFirst())
            timezone = TimeZone(identifier: timezoneString)!
            if let location = WeatherDataController.shared.location {
                locationLabel.text = location.name
            }
        } else {
            if let error = WeatherDataController.shared.error {
                self.displayError(error.localizedDescription)
                locationLabel.text = "No Data"
            }
            forecasts = []
        }
        
        tableView.reloadData()
        
        if let favourite = WeatherDataController.shared.favourite,
            let location = WeatherDataController.shared.location {
            favouriteButton.isFavourite = favourite.type == .sevenDay && favourite.location == location
        }
    }
    
    func displayError(_ error: String) {
        let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true)
    }
    
    func configure(cell: SevenDayForecastTableViewCell, forIndexPath indexPath: IndexPath) {
        let forecast = forecasts[indexPath.row]
        
        cell.dateLabel.text = "Unknown"
        cell.weatherImageView.image = UIImage(named: "default")
        cell.highTemperatureLabel.text = "--º"
        cell.lowTemperatureLabel.text = "--º"
        cell.descriptionLabel.text = ""
        cell.windDirectionLabel.text = "--º"
        cell.windSpeedLabel.text = "-- km/h"
        
        if let timezone = timezone {
            let time = Date(timeIntervalSince1970: TimeInterval(forecast.time))
            cell.dateLabel.text = WeatherDataController.shared.formatDayOfWeek(forDate: time, inTimeZone: timezone)
        }
        
        if let imageName = forecast.imageName {
            cell.weatherImageView.image = UIImage(named: imageName)
        }
        
        if let highTemperature = forecast.highTemperature {
            cell.highTemperatureLabel.text = "\(Int(highTemperature))º"
        }
        
        if let lowTemperature = forecast.lowTemperature {
            cell.lowTemperatureLabel.text = "\(Int(lowTemperature))º"
        }
        
        if let description = forecast.description {
            cell.descriptionLabel.text = description
        }
        
        if let windDirection = forecast.windBearing {
            cell.windDirectionLabel.text = "\(windDirection)º"
        }
        
        if let windSpeed = forecast.windSpeed {
            cell.windSpeedLabel.text = String(format: "%.1f km/h", windSpeed)
        }
    }
    
    func weatherDataDidFinishUpdate() {
        updateUI()
    }
    
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecasts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "7DayForecastCell", for: indexPath) as! SevenDayForecastTableViewCell
        configure(cell: cell, forIndexPath: indexPath)
        return cell
    }
    
    
    @IBAction func favouriteButtonTapped(_ sender: UIButton) {
        let feedbackGenerator = UISelectionFeedbackGenerator()
        feedbackGenerator.prepare()
        feedbackGenerator.selectionChanged()
        if sender.isSelected {
            WeatherDataController.shared.removeFavourite()
        } else {
            WeatherDataController.shared.setFavourite(forType: .sevenDay)
        }
        updateUI()
    }
    
    @IBAction func refreshButtonTapped(_ sender: UIBarButtonItem) {
        WeatherDataController.shared.updateWeatherData()
    }
    
}
