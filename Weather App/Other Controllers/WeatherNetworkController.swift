//
//  WeatherNetworkController.swift
//  Weather App
//
//  Created by Dean Mollica on 28/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

class WeatherNetworkController {
    private let apiKey = "a34f1f165656c8d0b8f6201f38b8e196"
    private let baseURL = URL(string: "https://api.darksky.net/forecast")!
    private let exclusions = ["minutely", "hourly", "alerts", "flags"]
    
    func fetchWeatherData(for location: Location, withUnits units: WeatherUnits, completion: @escaping (WeatherDataContainer?, Error?) -> Void) {
        let queries = [
            "exclude": self.exclusions.joined(separator: ","),
            "units": units.rawValue
        ]
        let requestURL = self.baseURL.appendingPathComponent(self.apiKey).appendingPathComponent(location.locationString()).withQueries(queries)!
        let task = URLSession.shared.dataTask(with: requestURL) { (data, response, error) in
            let decoder = JSONDecoder()
            if let data = data {
                do {
                    let decodedData = try decoder.decode(WeatherDataContainer.self, from: data)
                    completion(decodedData, nil)
                } catch {
                    completion(nil, error)
                }
            } else {
                completion(nil, error)
            }
        }
        task.resume()
    }
}
