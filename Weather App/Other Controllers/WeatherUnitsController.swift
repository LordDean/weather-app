//
//  WeatherUnitsController.swift
//  Weather App
//
//  Created by Dean Mollica on 21/8/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

struct WeatherUnitsController: Codable {
    var units: WeatherUnits
    
    private static let DocumentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    private static let ArchiveURL = DocumentsDirectoryURL.appendingPathComponent("units").appendingPathExtension("plist")
    
    static func loadUnits() -> WeatherUnitsController? {
        guard let unitsData = try? Data(contentsOf: ArchiveURL) else { return nil }
        let decoder = PropertyListDecoder()
        return try? decoder.decode(WeatherUnitsController.self, from: unitsData)
    }
    
    static func saveUnits(_ units: WeatherUnitsController) {
        let encoder = PropertyListEncoder()
        let unitsData = try? encoder.encode(units)
        try? unitsData?.write(to: ArchiveURL, options: .noFileProtection)
    }
}
