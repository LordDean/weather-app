//
//  WeatherDataController.swift
//  Weather App
//
//  Created by Dean Mollica on 4/8/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

protocol WeatherDataDelegate {
    func weatherDataDidFinishUpdate()
}


class WeatherDataController {
    static let shared = WeatherDataController()
    
    let weatherNetworkController = WeatherNetworkController()
    var delegate: WeatherDataDelegate?
    var weatherData: WeatherDataContainer?
    var error: Error?
    var favourite: Favourite? = Favourite.loadFavourite()
    var isLoading: Bool = false
    
    var units: WeatherUnits = WeatherUnitsController.loadUnits()?.units ?? .metric
    
    var location: Location? {
        didSet {
            if let newLocation = location {
                updateWeatherData(for: newLocation, withUnits: units)
            }
        }
    }
    
    private let lastUpdatedFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter
    }()
    
    private var dailyDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        return dateFormatter
    }()
    
    private var localTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, d MMM yyyy, h:mm a"
        return dateFormatter
    }()
    
    private var dayOfWeekFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, d MMM"
        return dateFormatter
    }()
    
    func updateWeatherData(for location: Location, withUnits units: WeatherUnits) {
        isLoading = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        weatherNetworkController.fetchWeatherData(for: location, withUnits: units) { (weatherDataContainer, error) in
            self.weatherData = weatherDataContainer
            self.error = error
            DispatchQueue.main.async {
                self.isLoading = false
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.delegate?.weatherDataDidFinishUpdate()
            }
        }
    }
    
    func updateWeatherData() {
        guard let location = location else { return }
        updateWeatherData(for: location, withUnits: units)
    }
    
    func formatLastUpdated(forDate date: Date) -> String {
        return lastUpdatedFormatter.string(from: date)
    }
    
    func formatDailyDate(forDate date: Date, inTimeZone timezone: TimeZone) -> String {
        dailyDateFormatter.timeZone = timezone
        return dailyDateFormatter.string(from: date)
    }
    
    func formatLocalTime(forDate date: Date, inTimeZone timezone: TimeZone) -> String {
        localTimeFormatter.timeZone = timezone
        return localTimeFormatter.string(from: date)
    }
    
    func formatDayOfWeek(forDate date: Date, inTimeZone timezone: TimeZone) -> String {
        dayOfWeekFormatter.timeZone = timezone
        return dayOfWeekFormatter.string(from: date)
    }
    
    func setFavourite(forType type: FavouriteType) {
        favourite = Favourite(location: location!, type: type)
        if let favourite = favourite {
            Favourite.saveFavourite(favourite)
        }
    }
    
    func removeFavourite() {
        favourite = nil
        Favourite.removeFavourite()
    }
    
    func setUnits(_ units: WeatherUnits) {
        self.units = units
        WeatherUnitsController.saveUnits(WeatherUnitsController(units: units))
    }
}
