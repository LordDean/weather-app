//
//  WeatherUnits.swift
//  Weather App
//
//  Created by Dean Mollica on 31/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

enum WeatherUnits: String, Codable {
    case metric = "si"
    case imperial = "us"
}
