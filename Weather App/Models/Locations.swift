//
//  Locations.swift
//  Weather App
//
//  Created by Dean Mollica on 28/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

struct Location: Codable, Equatable {
    let name: String
    let latitude: Double
    let longitude: Double
    
    func locationString() -> String {
        let latitudeString = String(format: "%.4f", latitude)
        let longitudeString = String(format: "%.4f", longitude)
        return "\(latitudeString),\(longitudeString)"
    }
}

let Locations: [Location] = [
    Location(name: "Melbourne", latitude: -37.8136, longitude: 144.9631),
    Location(name: "Cairo", latitude: 30.0488, longitude: 31.2437),
    Location(name: "London", latitude: 51.5073, longitude: -0.1276),
    Location(name: "Tokyo", latitude: 34.6969, longitude: 139.4049),
    Location(name: "Paris", latitude: 48.8566, longitude: 2.3515),
    Location(name: "Rome", latitude: 41.8933, longitude: 12.4829),
    Location(name: "Barcelona", latitude: 41.3829, longitude: 2.1774),
    Location(name: "New York", latitude: 40.7306, longitude: -73.9866),
    Location(name: "Copenhagen", latitude: 55.6867, longitude: 12.5701),
    Location(name: "Colombo", latitude: 6.935, longitude: 79.8538),
    Location(name: "Dublin", latitude: 53.3498, longitude: -6.2603),
    Location(name: "Palermo", latitude: 38.1112, longitude: 13.3524),
    Location(name: "Christchurch", latitude: -43.531, longitude: 172.6366),
    Location(name: "Beijing", latitude: 39.906, longitude: 116.3912),
    Location(name: "Reykjavik", latitude: 64.146, longitude: -21.9422),
    Location(name: "Moscow", latitude: 55.7507, longitude: 37.6177),
    Location(name: "Cape Town", latitude: -33.929, longitude: 18.4174),
    Location(name: "New Delhi", latitude: 28.6142, longitude: 77.2023),
    Location(name: "South Pole", latitude: -90, longitude: 0),
    Location(name: "Dubai", latitude: 25.075, longitude: 55.1888)
]
