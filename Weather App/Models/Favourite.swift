//
//  Favourite.swift
//  Weather App
//
//  Created by Dean Mollica on 7/8/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

struct Favourite: Codable {
    var location: Location
    var type: FavouriteType
    
    private static let DocumentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    private static let ArchiveURL = DocumentsDirectoryURL.appendingPathComponent("favourite").appendingPathExtension("plist")
    
    static func loadFavourite() -> Favourite? {
        guard let favouriteData = try? Data(contentsOf: ArchiveURL) else { return nil }
        
        let decoder = PropertyListDecoder()
        return try? decoder.decode(Favourite.self, from: favouriteData)
    }
    
    static func saveFavourite(_ favourite: Favourite) {
        let encoder = PropertyListEncoder()
        let favouriteData = try? encoder.encode(favourite)
        try? favouriteData?.write(to: ArchiveURL, options: .noFileProtection)
    }
    
    static func removeFavourite() {
        try? FileManager.default.removeItem(at: ArchiveURL)
    }
}
