//
//  WeatherDataBlock.swift
//  Weather App
//
//  Created by Dean Mollica on 28/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

struct WeatherDataBlock: Codable {
    let data: [WeatherDataPoint]
    
    enum CodingKeys: CodingKey {
        case data
    }
    
    init(from decoder: Decoder) throws {
        let valueContainer = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try valueContainer.decode([WeatherDataPoint].self, forKey: CodingKeys.data)
    }
}
