//
//  WeatherDataContainer.swift
//  Weather App
//
//  Created by Dean Mollica on 28/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

struct WeatherDataContainer: Codable {
    let timezone: String
    let currentWeather: WeatherDataPoint?
    let dailyWeather: WeatherDataBlock?
    
    enum CodingKeys: String, CodingKey {
        case timezone
        case currentWeather = "currently"
        case dailyWeather = "daily"
    }
    
    init(from decoder: Decoder) throws {
        let valueContainer = try decoder.container(keyedBy: CodingKeys.self)
        self.timezone = try valueContainer.decode(String.self, forKey: CodingKeys.timezone)
        self.currentWeather = try? valueContainer.decode(WeatherDataPoint.self, forKey: CodingKeys.currentWeather)
        self.dailyWeather = try? valueContainer.decode(WeatherDataBlock.self, forKey: CodingKeys.dailyWeather)
    }
}
