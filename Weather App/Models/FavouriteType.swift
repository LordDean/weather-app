//
//  FavouriteType.swift
//  Weather App
//
//  Created by Dean Mollica on 7/8/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

enum FavouriteType: String, Codable {
    case current = "CurrentConditionsViewController"
    case today = "TodaysForecastViewController"
    case sevenDay = "SevenDayViewController"
}
