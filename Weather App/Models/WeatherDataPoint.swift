//
//  WeatherDataPoint.swift
//  Weather App
//
//  Created by Dean Mollica on 28/7/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

struct WeatherDataPoint: Codable {
    let time: Int
    let currentTemperature: Double?
    let description: String?
    let highTemperature: Double?
    let lowTemperature: Double?
    let windBearing: Int?
    let windSpeed: Double?
    let imageName: String?
    
    enum CodingKeys: String, CodingKey {
        case time
        case currentTemperature = "temperature"
        case description = "summary"
        case highTemperature = "temperatureHigh"
        case lowTemperature = "temperatureLow"
        case windBearing
        case windSpeed
        case imageName = "icon"
    }
    
    init(from decoder: Decoder) throws {
        let valueContainer = try decoder.container(keyedBy: CodingKeys.self)
        self.time = try valueContainer.decode(Int.self, forKey: CodingKeys.time)
        self.currentTemperature = try? valueContainer.decode(Double.self, forKey: CodingKeys.currentTemperature)
        self.description = try? valueContainer.decode(String.self, forKey: CodingKeys.description)
        self.highTemperature = try? valueContainer.decode(Double.self, forKey: CodingKeys.highTemperature)
        self.lowTemperature = try? valueContainer.decode(Double.self, forKey: CodingKeys.lowTemperature)
        self.windBearing = try? valueContainer.decode(Int.self, forKey: CodingKeys.windBearing)
        self.windSpeed = try? valueContainer.decode(Double.self, forKey: CodingKeys.windSpeed)
        self.imageName = try? valueContainer.decode(String.self, forKey: CodingKeys.imageName)
    }
}
